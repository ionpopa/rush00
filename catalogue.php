<?php
session_start();

include 'var.php';
include 'head.php';

function print_number($row)
{
	$tot = 0;
	foreach ($_SESSION['panier'] as $elem)
	{
		if ($elem['id'] == $row['id'])
			$tot++;
	}
	return ($tot);
}
?>

<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="rush.css" />
		<title>42</title>
	</head>
	<body>

<?php
$con=mysqli_connect($GLOBALS['server'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['dbname']);
if (mysqli_connect_errno())
{
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
$prix = "";
$prix_get = "";
$cat = "";
$cat_get = "";
$nom = "";
$nom_get = "";

if ($_GET['prix'] and $_GET['prix'] == 'desc')
{
	$prix_get = htmlspecialchars($_GET['prix']);
	$prix = "ORDER BY price DESC";
}
else if ($_GET['prix'] and $_GET['prix'] == 'asc')
{
	$prix_get = htmlspecialchars($_GET['prix']);
	$prix = "ORDER BY price ASC";
}

if ($_GET['cat'])
{
	$cat = htmlspecialchars($_GET['cat']);
	$cat = "WHERE categorie1='".$cat."' OR categorie2='".$cat."' OR categorie3='".$cat."'";
	$cat_get = htmlspecialchars($_GET['cat']);
	$res = mysqli_query($con,"SELECT * FROM ".$GLOBALS['prod']." ".$cat." ".$prix."");
}
else if($_GET['nom'])
{
	$nom = htmlspecialchars($_GET['nom']);
	$nom = "WHERE name='".$nom."'";
	$nom_get = htmlspecialchars($_GET['nom']);
	$res = mysqli_query($con,"SELECT * FROM ".$GLOBALS['prod']." ".$nom." ".$prix."");
}
else
	$res = mysqli_query($con,"SELECT * FROM ".$GLOBALS['prod']." ".$cat." ".$prix."");
echo "<p id='order_price'>Prix : <a href='catalogue.php?prix=desc&cat=".htmlspecialchars($_GET['cat'])."&nom=".htmlspecialchars($_GET['nom'])."'> ▲ <a href='catalogue.php?prix=asc&cat=".htmlspecialchars($_GET['cat'])."&nom=".htmlspecialchars($_GET['nom'])."'> ▼ </a></p>";
while ($row = mysqli_fetch_array($res))
{
	echo "<article>";
	echo "<p class='name'>".$row['name']."</p>";
	echo "<a href='add.php?id=".$row['id']."&prix=".$prix_get."&cat=".$cat_get."&nom=".$nom_get."'><img class='add' src='img/add.png' alt='ajouter' title='ajouter' /></a>";
	echo "<p class='prix'>".$row['price']." € ";
	if (print_number($row) != 0)
		echo "<span class='number'> x".print_number($row)."</span><br />";
	echo "<p class='categorie'><a class='link_".$row['categorie1']."' href='catalogue.php?cat=".$row['categorie1']."'>".strtoupper($row['categorie1'])."</a></p>";
	echo "<p class='description'>".$row['description']."</p>";
	echo "</p>";
	echo "<br/ >";
	echo "</article>";
}
	echo "<div><br /><br /><br /><br /></div>";

mysqli_close($con);
?>
<?php
include 'footer.php';
?>
	</body>
</html>
