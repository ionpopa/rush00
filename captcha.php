<?php

session_start();

if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');


function getCode($length) 
{
	$chars = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
	$code = '';
	for ($i=0; $i<$length; $i++) {
		$code .= $chars{ mt_rand( 0, strlen($chars) - 1 ) };
	}
	return $code;
}

function random($tab) {
	return $tab[array_rand($tab)];
}


$code = getCode(5);

$_SESSION['captcha'] = md5($code);


$char1 = substr($code,0,1);
$char2 = substr($code,1,1);
$char3 = substr($code,2,1);
$char4 = substr($code,3,1);
$char5 = substr($code,4,1);


$image = imagecreatefrompng('img/captcha.png');


$colors = array ( imagecolorallocate($image, 131, 154, 255),
	imagecolorallocate($image,  89, 186, 255),
	imagecolorallocate($image, 155, 190, 214),
	imagecolorallocate($image, 255, 128, 234),
	imagecolorallocate($image, 255, 123, 123) );

imagettftext($image, 28, -10, 0, 37, random($colors), ABSPATH .'/', $char1);
imagettftext($image, 28, 20, 37, 37, random($colors), ABSPATH .'/', $char2);
imagettftext($image, 28, -35, 55, 37, random($colors), ABSPATH .'/', $char3);
imagettftext($image, 28, 25, 100, 37, random($colors), ABSPATH .'/', $char4);
imagettftext($image, 28, -15, 120, 37, random($colors), ABSPATH .'/', $char5);

header('Content-Type: image/png');


imagepng($image);

imagedestroy($image);
?>