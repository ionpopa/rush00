<?php

include 'var.php';

function calc_number()
{
	return count($_SESSION['panier']);
}

?>
<header id="header">
	<div id="ecommerce"><img id="img_ecommerce" src="img/logo42.jpg"></div>
	<div id="pannier"><span id="nb_item"><?php echo calc_number(); ?></span><a href="panier.php"><img id="img_pannier" src="img/panier.png"></a></div>
	<div id="account">
<?php
if ($_SESSION["loggued_on_user"])
{
	$servername = $GLOBALS['server'];
	$username = $GLOBALS['user'];
	$password = $GLOBALS['pass'];
	$dbname = $GLOBALS['dbname'];

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if (mysqli_connect_errno())
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	$res = mysqli_query($conn,"SELECT login FROM ".$GLOBALS['guest']." WHERE id='".$_SESSION['loggued_on_user']."'");
	$row = mysqli_fetch_array($res);
?>
			<a class="compte" href="logout.php">Logout</a><br />
			<a class="compte" href="delete.php">Delete account</a><br />
			<span style='color:white;'>Login : <?php echo $row['login']; ?></span><br />
<?php
	if ($_SESSION['loggued_on_user'] != "" and $_SESSION['member'] == 'admin')
		echo "<br /><a class='compte' href='admin.php'>Administration</a>";
?>
<?php
}
else
{
?>
			<a class="compte" href="login.php">Login</a><br /><br />
			<a class="compte" href="create.php">Create an account</a>
<?php
}
?>
	</div>
</header>
<div id="separation"></div>
<div id="menu">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="catalogue.php">Catalogue</a>
			<ul>
				<li><a href='catalogue.php?cat=souris'>Souris</a></li>
				<li><a href='catalogue.php?cat=clavier'>Clavier</a></li>
				<li><a href='catalogue.php?cat=autre'>Autre</a></li>
			</ul>
		</li>
		<li><a href="contact.php">Contact us</a></li>
	</ul>
</div>
<br />
