<?php

include 'var.php';

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];

$link = mysql_connect($servername, $username, $passwd);
if (!$link) {
	die('Connexion impossible : ' . mysql_error());
}
$sql = 'CREATE DATABASE '.$dbname.'';
mysql_query($sql, $link);

mysqli_close($link);

$conn = mysqli_connect($servername, $username, $passwd, $dbname);
if (!$conn)
	die("Connection failed: " . mysqli_connect_error());
$sql_users = "CREATE TABLE IF NOT EXISTS ".$GLOBALS['guest']." (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	login VARCHAR(30) NOT NULL,
	passwd TEXT NOT NULL,
	member VARCHAR(30) NOT NULL
	)";

$sql_products = "CREATE TABLE IF NOT EXISTS ".$GLOBALS['prod']." (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	name VARCHAR(30) NOT NULL,
	brand VARCHAR(30) NOT NULL,
	price INT(6),
	categorie1 VARCHAR(30) NOT NULL,
	categorie2 VARCHAR(30) NOT NULL,
	categorie3 VARCHAR(30) NOT NULL,
	description TEXT NOT NULL
	)";

$sql_panier = "CREATE TABLE IF NOT EXISTS ".$GLOBALS['panier']." (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	id_user INT(6) NOT NULL,
	id_prod INT(6) NOT NULL,
	number INT(6) NOT NULL,
	done VARCHAR(255) NOT NULL
	)";

mysqli_query($conn, $sql_users);
mysqli_query($conn, $sql_products);
mysqli_query($conn, $sql_panier);

$admin_p = hash("whirlpool", 'admin');
$count = mysqli_query($conn,"SELECT * FROM ".$GLOBALS['guest']." WHERE login='admin'");
if (!mysqli_num_rows($count))
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['guest']." (`id`, `firstname`, `lastname`, `login`, `passwd`, `member`) VALUES (NULL, 'admin','admin','admin','".$admin_p."', 'admin')");
$count = mysqli_query($conn,"SELECT * FROM ".$GLOBALS['prod']." WHERE name='souris fummee' OR name='3GB de RAM' OR name='KAV'");
if (!mysqli_num_rows($count))
{
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['prod']." (`id`, `name`, `brand`, `price`, `categorie1`, `categorie2`, `categorie3`, `description`) VALUES (NULL, 'Souris fummee','Logitech','20','souris', 'Autre', 'nope', 'Souris de bonne qualite, sans fil a detection optique. Son revetement grippant vous permettra une prise en main remarquable')");
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['prod']." (`id`, `name`, `brand`, `price`, `categorie1`, `categorie2`, `categorie3`, `description`) VALUES (NULL, '3GB de RAM','Kingston','30','autre', 'Autre', 'nope', 'La marque Kingston n a plus a faire ses preuves et en voici encore un exemple avec une serie de Ram qui rendre une seconde jeunesse a vos PC les plus obsoletes')");
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['prod']." (`id`, `name`, `brand`, `price`, `categorie1`, `categorie2`, `categorie3`, `description`) VALUES (NULL, 'KAV','kaspersky','30','autre', 'Autre', 'nope', 'Logiciel anti malware / protection totale contre les virus les plus robustes / systeme de diagnostic en ligne / efficacite 100% voici le dernier chef d oeuvre du geant kasperski')");
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['prod']." (`id`, `name`, `brand`, `price`, `categorie1`, `categorie2`, `categorie3`, `description`) VALUES (NULL, 'Razer Tarantula','Razer','115','clavier', 'autre', 'nope', 'Le nec plus ultra des claviers de competitions, encore une prouesse technologique sortie des usines de Razer qui marque definitivement son territoire en lancant ce nouveau clavier. Pression des touches reglables a la volee / multi retroeclairage / detection digitale, ce bijou est sans contest le plus aboutti de sa gamme.')");
}

mysqli_close($conn);
?>
