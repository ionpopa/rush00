<?php

session_start();

include 'head.php';
?>

<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="rush.css" />
	<title>42</title>
</head>
<body>

	<?php
	$tot = 0;
	foreach($_SESSION['panier'] as $elem)
		$tot = $tot + $elem['price'];
	echo "<div id='liste'>";
	if ($tot != 0)
		echo "<ul id='achat'>Tu as achete : <br /><br />";
	else
		echo "<ul id='achat'>Ton panier est vide<br /><br />";

	$array = array();
	$elem_num = 0;
	$i = 0;
	while ($i < count($_SESSION['panier']))
	{
		$elem_num = 0;
		if (in_array($_SESSION['panier'][$i]['id'], $array) == False)
		{
			array_push($array, $_SESSION['panier'][$i]['id']);
			$j = 0;
			while ($j < count($_SESSION['panier']))
			{
				if ($_SESSION['panier'][$j] == $_SESSION['panier'][$i])
					$elem_num++;
				$j++;
			}
			echo "<li>".strtoupper($_SESSION['panier'][$i]['categorie1'])." - ".$_SESSION['panier'][$i]['brand']." : ".$_SESSION['panier'][$i]['price']." €  x".$elem_num."</li>";
		}
		$i++;
	}
	echo "<br />Total : ".$tot." €<br />";
	if ($_SESSION['loggued_on_user'] != "" and ($_SESSION['member'] == "user" or $_SESSION['member'] == 'admin'))
	{
		if ($tot > 0)
			echo "<p id='validate'><a href='validation.php'>Valide ton panier</a></p>";
	}
	else
		echo "Si tu veux valider ton panier <br /><a href='login.php'>Connecte toi</a> ou <a href='create.php'>Creer un compte</a>";
	?>
</div>
<p id='revenir'>
	<br />
	<a href='catalogue.php'>Revenir aux achats</a><br /><br />
	<a href='reset.php'>Vider le panier</a><br />
</p>
<?php
include 'footer.php'
?>

</body>
</html>
